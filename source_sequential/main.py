from Mower import *
from utils import *


def run():
    f = open(Files().input_file)
    output = open(Files().output_file,'w')

    mower_data = f.readlines()[1:]
    lawn = Lawn()
    i=0
    while i < len(mower_data):
        mower_info = mower_data[i].split(' ')
        coordinates = to_int(mower_info[0:2])
        direction = mower_info[2].split('\n')[0]
        action = mower_data[i+1]
        mower = Mower(coordinates, direction, action, lawn)   
        mower.move()   
        output.write(str(mower.coordinates[0])+' '+str(mower.coordinates[1])+' '+mower.direction+'\n')
        i+=2
    f.close()
    output.close()


if __name__ == "__main__": 
    run()
              
