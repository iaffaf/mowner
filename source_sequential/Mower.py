
import os
from utils import *


class Files:
    def __init__(self):
        self.input_file =  "input_file.txt"
        self.output_file = "output_file.txt"

class Lawn:
    def _get_lawn_size(self):
        f = open(Files().input_file)
        size = to_int(f.readline().split(' '))
        f.close()
        return size
    
    def __init__(self):
        self.X = self._get_lawn_size()[0]
        self.Y = self._get_lawn_size()[1]

class Mower:
    Direction = ['N','E','S','W']

    def __init__(self, coordinates, direction, action, lawn):
        self.coordinates = coordinates
        self.direction = direction
        self.action = action
        self.lawn = lawn
        
    def move(self):
        for elem in self.action:
            if elem == 'R':
                self.turn_right()
            if elem == 'L':
                self.turn_left() 
            if elem == 'F':
                self.move_forward()
            
    def turn_left(self):
        ind = Mower.Direction.index(self.direction)
        if ind > 0:
            self.direction = Mower.Direction[ind-1]
        else:
            self.direction = Mower.Direction[len(Mower.Direction)-1]
                   
    def turn_right(self):
        ind = Mower.Direction.index(self.direction)
        if ind == len(Mower.Direction)-1:
            self.direction = Mower.Direction[0]
        else:
            self.direction = Mower.Direction[ind+1]
    
    def move_forward(self):
        x = self.coordinates[0]
        y = self.coordinates[1]
        
        if self.direction == 'N':
            self.coordinates[1] = min(y+1, self.lawn.X)
        if self.direction == 'E':        
            self.coordinates[0] = min(x+1, self.lawn.Y)
        if self.direction == 'S':
            self.coordinates[1] = max(y-1, 0)
        if self.direction == 'W':
            self.coordinates[0] = max(x-1, 0)
            
