from Mower import *
from utils import *


def run():
    f = open(Files().input_file)
    output = open(Files().output_file,'w')

    mower_data = f.readlines()[1:]
    mowers = []
    n = len(mower_data)
    lawn = Lawn(n // 2)
    i = 0
    label = 1
    while i < n:
        mower_info = mower_data[i].split(' ')
        coordinates = to_int(mower_info[0:2])
        direction = mower_info[2].split('\n')[0]
        action = mower_data[i+1]
        mower = Mower(coordinates, direction, action, lawn, label)   
        mowers.append(mower)
        mower.run()
        i += 2
        label += 1
    
    while (lawn.mowers != 0):
        continue 

    for mower in mowers:
        mower.write_mower_final_data(output)    

    f.close()
    output.close()



if __name__ == "__main__": 
    run()
              