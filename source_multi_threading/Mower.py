import os
from utils import *
import threading

class Files:
    def __init__(self):
        self.input_file =  "input_file.txt"
        self.output_file = "output_file.txt"

class Lawn:
    def _get_lawn_size(self):
        f = open(Files().input_file)
        size = to_int(f.readline().split(' '))
        f.close()
        return size
    
    def __init__(self, n):
        self.X = self._get_lawn_size()[0]
        self.Y = self._get_lawn_size()[1]
        self.grid = [[0 for i in range(self.Y + 1)] for j in range(self.X + 1)] 
        self.lock = threading.Lock()
        self.mowers = n

class Mower:
    Direction = ['N','E','S','W']

    def __init__(self, coordinates, direction, action, lawn, label):
        self.coordinates = coordinates
        self.direction = direction
        self.action = action
        self.lawn = lawn
        self.label = label
        self.thread = threading.Thread(target=self.move)

    def run(self):
        self.thread.run()    
        
    def move(self):
        for elem in self.action:
            if elem == 'R':
                self.turn_right()
            if elem == 'L':
                self.turn_left() 
            if elem == 'F':
                self.move_forward()
        self.lawn.mowers -= 1 
            
    def turn_left(self):
        ind = Mower.Direction.index(self.direction)
        if ind > 0:
            self.direction = Mower.Direction[ind-1]
        else:
            self.direction = Mower.Direction[len(Mower.Direction)-1]
                   
    def turn_right(self):
        ind = Mower.Direction.index(self.direction)
        if ind == len(Mower.Direction)-1:
            self.direction = Mower.Direction[0]
        else:
            self.direction = Mower.Direction[ind+1]
    
    def move_forward(self):
        x = self.coordinates[0]
        y = self.coordinates[1]
        
        if self.direction == 'N':
           y = y + 1
        if self.direction == 'E':        
            x = x + 1
        if self.direction == 'S':
            y = y - 1
        if self.direction == 'W':
            x = x - 1

        if self.is_valid_move(x , y):
            self.lawn.lock.acquire()
            self.lawn.grid[x][y] = self.label
            self.lawn.grid[self.coordinates[0]][self.coordinates[1]] = 0
            self.coordinates[0] , self.coordinates[1] = x , y
            self.lawn.lock.release()

    def is_valid_move(self, x , y):
        if x > self.lawn.X or x < 0 or y < 0 or y > self.lawn.Y:
            return False

        return self.lawn.grid[x][y] == 0

    def write_mower_final_data(self, output):
        output.write(str(self.coordinates[0])+' '+str(self.coordinates[1])+' '+self.direction+'\n')

            
